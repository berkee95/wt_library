function clearFields() {
   var frm = document.getElementsByName('input-form')[0];
   frm.reset();  // Reset
   return false; // Prevent page refresh
}


function showBooks(){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:8082/show", true); // POST OF GET
    xhr.setRequestHeader("Content-Type", "application/json")
    xhr.send();

    xhr.onreadystatechange = function (){
        
        if(xhr.readyState == 4){
            var txt = "";
            var data = this.responseText;
            makeTable(data);
        } 
    }
}

function searchBook(){
    var xhr = new XMLHttpRequest();
    var input = document.getElementById("search_input").value;

    xhr.open("GET", "http://localhost:8082/searchbook/"+input, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();

    xhr.onreadystatechange =  function(){
        if(xhr.readyState == 4){
            var data = this.responseText;
            makeTable(data);
        }
    }
}

function makeTable(data){
    var txt = "";
    var books = JSON.parse(data);
    txt += "<thead> <tr class='w3-light-grey'> <th>Book title</th> <th>Author</th> <th>ISBN</th> <th>Tags</th> </tr> </thead>";
    txt += "<table border='1'>";

    for (x in books) {
        if (books[x]["tags"] != "[]"){
            var tags= "";
            for (z in books[x]["tags"]){
                tags += books[x]["tags"][z]["text"] ;
                if (z< books[x]["tags"].length-1){
                    tags +=  ",  ";
                } else{
                    tags +=  ".";
                }
            }
        }
        txt += "<tr class='w3-light-grey w3-hover-light-green' onclick='bookPage(" + JSON.stringify(books[x]) + ")'><td>" + books[x].title + "</td>"+"<td>"+ books[x].author+ "</td>"+"<td>" + books[x].isbn + "</td>"+"<td>" + tags +  "</td></tr>";
    }
    txt += "</table>"
    document.getElementById("bookTable").innerHTML = txt;
}



function bookPage(data){
    book = data;
    
    if (document.getElementById("admin").classList.contains("active")){
        window.location.href= "book.html?"+book.id;
        var opened = window.open("book.html?"+book.id);
    } else if (document.getElementById("user").classList.contains("active")){
        window.location.href= "bookUser.html?"+book.id;
        var opened = window.open("bookUser.html?"+book.id);
        
    } 

    opened.onload = function(){
        opened.document.getElementById("bookInfo").innerHTML= "Titel: " + book.title + " Auteur: "+ book.author + " ISBN: "+book.isbn;
    }
    return data; 
}

function reserveBook(){ 
    var splitted_string = window.location.href.split('book.html?');
    var bookId = splitted_string[splitted_string.length-1];
    var xhr = new XMLHttpRequest();  
    xhr.open("GET", "http://localhost:8082/reserve/"+bookId, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();

    alert("Er is een exemplaar gereserveerd!");
    
}

function addBook(){
    var xhr = new XMLHttpRequest();

    var inputTitle = document.getElementById("title").value;
    var inputAuthor = document.getElementById("author").value;
    var inputIsbn = document.getElementById("isbn").value;
    var inputTags = document.getElementById("tags").value;

    var tagArray = inputTags.split(',');

    var data1 = {title: inputTitle, author: inputAuthor, isbn: inputIsbn};
    var data2 = []
   
    for(t in tagArray){
        while(tagArray[t].startsWith(" ")){
            tagArray[t] = tagArray[t].substring(1);
        }  
    
        while(tagArray[t].endsWith(" ")){
            tagArray[t] = tagArray[t].substring(0,tagArray[t].length-1);
        }
        data2.push({
           "text":tagArray[t]
        });

    }
    
    var combi = {book: data1, tags: data2}
    
    myJSON = JSON.stringify(combi);

    xhr4 = new XMLHttpRequest();
    xhr4 = xhr.open("POST", "http://localhost:8082/savebookandtags", true);
    xhr.setRequestHeader("Content-Type", "application/json")
    xhr.send(myJSON);
    }

////////////////////////////////////////////////// hoort bij book.html

function addCopy(){    
    var splitted_string = window.location.href.split("book.html?");
    var bookId = splitted_string[splitted_string.length-1];
    console.log("id"+ bookId);
    xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:8082/addbookcopy/"+bookId, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(); 

    alert("Je hebt een exemplaar van dit boek toegevoegd.")
}

function addUser(){    
    var inputFirstname = document.getElementById("firstname").value;
    var inputLastname = document.getElementById("lastname").value;
    var inputUsername = document.getElementById("username").value;
    var inputEmail = document.getElementById("email").value;
    var inputPassword = document.getElementById("password").value;

    console.log(inputEmail);
    var data = {firstname:inputFirstname, lastname:inputLastname, username:inputUsername,email:inputEmail,password:inputPassword};
    myJSON = JSON.stringify(data);
    xhr = new XMLHttpRequest();
    xhr.open("POST", "http://localhost:8082/createUser", true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(myJSON);
}


function lendBook(){ 
    var userId = document.getElementById("userId").value;
    console.log(userId);
    var xhr = new XMLHttpRequest();  
    xhr.open("GET", "http://localhost:8082/searchUserById/"+userId, true);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();


    xhr.onreadystatechange =  function(){
        if(xhr.readyState == 4){
            var user = this.responseText;
            var splitted_string = window.location.href.split('book.html?');
            var bookId = splitted_string[splitted_string.length-1];
            console.log(user);
            if (user!= []){
                var xhr2 = new XMLHttpRequest();  
                xhr2.open("GET", "http://localhost:8082/addOrder/"+bookId+"/"+userId+"/{dateBorrowed}/{dateReturned}/", true);
                xhr2.setRequestHeader('Content-Type', 'application/json');
                xhr2.send();
            
                alert("Er is een exemplaar uitgeleend");
            } else{
                alert("Dit is geen geldige user, je kunt niet een boek uitlenen.")
            }
            
        } 
    }     
}

function showReservedCopies(){
    var splitted_string = window.location.href.split("book.html?");
    var bookId = splitted_string[splitted_string.length-1];
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "http://localhost:8082/showReservedCopies/"+bookId, true); // POST OF GET
    xhr.setRequestHeader("Content-Type", "application/json")
    xhr.send();

    console.log('heyo');
    xhr.onreadystatechange = function (){
        
        if(xhr.readyState == 4){
            var txt = "";
            var data = this.responseText;
            makeReservedCopiesTable(data);
        } 
    }
}

function makeReservedCopiesTable(data){
    var txt = "";
    var copies = JSON.parse(data);
    txt += "<thead> <tr class='w3-light-grey'> <th>ID</th> <th>Reserveerder</th> <th>Uitlenen</th> </thead>";
    txt += "<table border='1'>";
    console.log(copies);
    for (x in copies) {
        console.log(copies[x]);
        console.log(copies[x].id);
        txt += "<tr class='w3-light-grey w3-hover-light-green' ><td>" + copies[x].id + "</td>"+"<td>Naampje</td> <td onclick = 'lendBook()'>Leen boek uit</td></tr>";
    }
    txt += "</table>"
    document.getElementById("reservedCopiesTable").innerHTML = txt;
}
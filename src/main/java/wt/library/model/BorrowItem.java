package wt.library.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class BorrowItem {
    @Id
    @GeneratedValue
    private long id;

    @Column(length = 50)
    private long bookID;
    @Column(length = 100)
    private long userID;
    //Misschien date Object importen
    @Column(length = 100)
    private String dateBorrowed;
    //Misschien date Object importen
    @Column(length = 100)
    private String dateReturned;
    //Eventueel bool for returned of via dateReturned

   
    /**
     * @return the id
     */
    public long getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }
    /**
     * @return the bookID
     */
    public long getBookID() {
        return bookID;
    }
    /**
     * @param bookID the bookID to set
     */
    public void setBookID(long bookID) {
        this.bookID = bookID;
    }
    /**
     * @return the userID
     */
    public long getUserID() {
        return userID;
    }
    /**
     * @param userID the userID to set
     */
    public void setUserID(long userID) {
        this.userID = userID;
    }
    /**
     * @return the dateBorrowed
     */
    public String getDateBorrowed() {
        return dateBorrowed;
    }
    /**
     * @param dateBorrowed the dateBorrowed to set
     */
    public void setDateBorrowed(String dateBorrowed) {
        this.dateBorrowed = dateBorrowed;
    }
    /**
     * @return the dateReturned
     */
    public String getDateReturned() {
        return dateReturned;
    }
    /**
     * @param dateReturned the dateReturned to set
     */
    public void setDateReturned(String dateReturned) {
        this.dateReturned = dateReturned;
    }


}


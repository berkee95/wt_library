package wt.library.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.*;

@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(length = 50)
    private String isbn;
    @Column(length = 100)
    private String title;
    @Column (length = 100)
    private String author;


    
    // @ManyToMany
    // @JoinTable(
    // name = "course_like", 
    // joinColumns = @JoinColumn(name = "student_id"), 
    // inverseJoinColumns = @JoinColumn(name = "course_id"))
    // Set<Course> likedCourses;

    public Boolean equals(Book book1, Book book2){
        // if (book1.getId() == book2.getId()){
        //     return true;
        // }

        if (book1.getTitle() == book2.getTitle() && book1.getAuthor() == book2.getAuthor() && book1.getIsbn()== book2.getIsbn()){
            return true;
        } else{
            return false;
        }
        
    }

    @ManyToMany
    private List<Tag> tags;

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void addTag(Tag tag){
        tags.add(tag);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author){
        this.author = author;
    }


}

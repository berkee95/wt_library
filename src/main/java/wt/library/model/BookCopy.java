package wt.library.model;

import javax.persistence.*;

	@Entity
	public class BookCopy { // extends Book{ meegeven van zit in een has relatie 
	    @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private long id;


		@Column(length = 50)
		private boolean borrowed;

		@Column(length = 50)
		private boolean reserved;

		@ManyToOne
		private Book book;

		public Book getBook(){
			return book;
		}
		public void setBook(Book book){
			this.book = book;
		}


		public boolean getBorrowed(){
			return borrowed;
		}
		public void setBorrowed(boolean borrowed){
			this.borrowed = borrowed;
		}

		public boolean getReserved(){
			return reserved;
		}
		public void setReserved(boolean reserved){
			this.reserved = reserved;
		}

		/**
		 * @return the id
		 */
		public long getId() {
			return id;
		}

		/**
		 * @param id the id to set
		 */
		public void setId(long id) {
			this.id = id;
		}

	    
	    
	}





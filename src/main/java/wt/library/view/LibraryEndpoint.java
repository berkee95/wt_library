package wt.library.view;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import wt.library.controller.BookCopyService;
import wt.library.controller.BookService;
import wt.library.controller.BorrowItemService;
import wt.library.controller.TagService;
import wt.library.controller.UserService;
import wt.library.model.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import wt.library.utility.Wrapper;
import wt.library.utility.WrapperBorrowItem;

@RestController
public class LibraryEndpoint {
    @Autowired
    private BorrowItemService borrowItemService;

    @Autowired
    private BookService bookService;

    @Autowired
    private BookCopyService bookCopyService;

    @Autowired
    private TagService tagService;

    @Autowired
    private UserService userService;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    @GetMapping("borrowItem/{id}")
    public BorrowItem findBorrowItemById(@PathVariable int id){
        BorrowItem borrowItem = borrowItemService.findBorrowItemById(id);
        return borrowItem;
    }

    @GetMapping("addOrder/{bookID}/{userID}/{dateBorrowed}/{dateReturned}")
    public void saveBorrowItem(@PathVariable int bookID,@PathVariable int userID,@PathVariable String dateBorrowed, @PathVariable String dateReturned){
        BorrowItem borrowItem = new BorrowItem();
        borrowItem.setBookID(bookID);
        borrowItem.setUserID(userID);
        borrowItem.setDateBorrowed(dateBorrowed);
        borrowItem.setDateReturned(dateReturned);
        borrowItemService.saveBorrowItem(borrowItem);
    }

    /*
    @GetMapping("addUserToBorrowItem/{borrowId}/{userId}")
    public void addUserToBorrowItem(@PathVariable long borrowId, @PathVariable long userId){
        userService.addUserToBorrowItem(borrowId, userId);
    }
    */


    // @PostMapping("borrowbook")
    // public void saveBorrowItem(@RequestBody WrapperBorrowItem wrapper){
    //     BorrowItem borrowItem = wrapper.borrowItem;


    //     User user = wrapper.user;
    //     borrowItemService.saveBorrowItem(borrowItem);
    //     //userService.addUserToBorrowItem(borrowItem.getId(), user.getId());
    // }

//Book
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    @PostMapping("savebook")
    public void saveBook(@RequestBody Book book){
        bookService.saveBook(book);
    }

    @GetMapping("show")
    public Collection <Book> showBooks(){
        Collection <Book> allBooks = bookService.findAllBooks();
        return allBooks;    
    }

    @GetMapping("getbook/{id}")
    public Book getBookById(@PathVariable int id){
        Book book = bookService.findBookById(id);
        return book;    
    }  
 
    @PostMapping("savebookandtags")
    public void saveBookAndTags(@RequestBody Wrapper wrapper){
        Book book = wrapper.book;
        List<Tag> tagList = wrapper.tags;
        
        bookService.saveBook(book);
        book.setTags(new ArrayList<Tag>());

        for (int i =0; i<tagList.size(); i++){
            Tag tag = tagList.get(i);
            tagService.saveTag(tag);
            bookService.addTagToBook(tag.getId(), book.getId());
            bookService.saveBook(book);
        } 
    } 
    
    @GetMapping("searchbook/{input}")
    public Collection <Book> searchBook(@PathVariable String input){
        Collection <Book> booksByTitleOrAuthor = bookService.findBookByTitleOrAuthor(input, input);
        Collection <Book> bookUnion;
        Tag tag = searchTagByText(input);

        if(tag!=null){
            System.out.println(tag.getId());
            List <Book> booksByTag = tag.getBooks();

            bookUnion = CollectionUtils.union(booksByTitleOrAuthor,booksByTag);
        } else{
            bookUnion = booksByTitleOrAuthor ;
        }
        return bookUnion;
    }

    @GetMapping("reserve/{id}")
    public void reserveCopy(@PathVariable long id){
        User user = new User();
        Book book = bookService.findBookById(id);
        bookService.reserveBook(book, user);
       
    }


// BookCopy
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    @GetMapping("addbookcopy/{bookid}")
    public void saveBookCopy(@PathVariable int bookid){
        Book book = bookService.findBookById(bookid);
        BookCopy copy = new BookCopy();
        copy.setReserved(false);
        copy.setBorrowed(false);
        copy.setBook(book);
        bookCopyService.savebookCopy(copy);
    }

    @GetMapping("showReservedCopies/{bookid}")
    public List<BookCopy> showReservedCopies(@PathVariable long bookid){
        List<BookCopy> bookCopies = bookCopyService.showReservedCopies(bookid);
        return bookCopies;
    }
    

 
    
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

    @PostMapping("createTag")
    public void createTag(@RequestBody Tag tag){
        tagService.saveTag(tag);
    }
    
    @GetMapping("addBookToTag/{tagId}/{bookId}")
    public void addBookToTag(@PathVariable long tagId, @PathVariable long bookId){
        System.out.println(tagId);
        System.out.println(bookId);
        bookService.addTagToBook(tagId, bookId);
    }

    @GetMapping("searchTagByText/{text}")
    public Tag searchTagByText(@PathVariable String text){
        return tagService.findTagByText(text);
    }

    @GetMapping("searchTagById/{id}")
    public Tag searchTagById(@PathVariable long id){
        return tagService.findTagById(id);
    }

    //User
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
    @PostMapping("createUser")
    public void createUser(@RequestBody User user){
        System.out.println(user);
        userService.saveUser(user);
    }

    @GetMapping("searchUserById/{id}")
    public User searchUserById(@PathVariable long id){
        
        return userService.findUserById(id);
    }
    

}

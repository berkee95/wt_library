package wt.library.controller;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import wt.library.model.User;
import wt.library.model.BorrowItem;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepo;

    public void saveUser(User user){
        userRepo.save(user);
    }
    @Autowired 
    private BorrowItemService borrowItemService;

    public User findUserById(long id){
        Optional<User> userOptional = userRepo.findById(id);
        System.out.println(userOptional);
        if(userOptional.isPresent()){
            System.out.println(userOptional.get());
            return userOptional.get();
        }
        return null;
    }

    public void addUserToBorrowItem(long borrowId, long userId){
       
        BorrowItem borrowItem = borrowItemService.findBorrowItemById(borrowId);
        User user = findUserById(userId);
        borrowItem.setUserID(userId);
        
        userRepo.save(user); 
    }
}

package wt.library.controller;

import org.springframework.data.jpa.repository.JpaRepository;

import wt.library.model.BorrowItem;

public interface BorrowItemRepository extends JpaRepository<BorrowItem, Long>{

}

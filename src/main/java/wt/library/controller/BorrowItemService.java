package wt.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;
import wt.library.model.BorrowItem;


@Service
public class BorrowItemService {
    @Autowired
    private BorrowItemRepository BorrowItemRepo;
    // maak een methode aan die een object met bepaalde id uit de database haalt
    // maak een methode die een boek opslaat in de database

    public BorrowItem findBorrowItemById(long id){
        Optional<BorrowItem> BorrowItemOptional = BorrowItemRepo.findById(id);
        if(BorrowItemOptional.isPresent()){
            return BorrowItemOptional.get();
        }
        return null;
    }
    /*
    public BorrowItem findBorrowItemByBorrowItemName(String BorrowItemname){
        Optional<BorrowItem> BorrowItemOptional = BorrowItemRepo.findBy(BorrowItemname);
        if(BorrowItemOptional.isPresent()){
            return BorrowItemOptional.get();
        }
        return null;
    }
    */

    public void saveBorrowItem(BorrowItem BorrowItem){
        BorrowItemRepo.save(BorrowItem);
    }
}

package wt.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wt.library.model.Book;
import wt.library.model.Tag;

import java.util.List;
import java.util.Optional;

@Service
public class TagService {
    @Autowired
    private BookService bookService;

    @Autowired
    private TagRepository tagRepo;

    @Autowired BookRepository bookRepo;

    public Tag findTagById(long id){
        Optional<Tag> tagOptional = tagRepo.findById(id);
        if(tagOptional.isPresent()){
            return tagOptional.get();
        }
        return null;
    }

    public Tag findTagByText(String text){
        List<Tag> tagList = tagRepo.findDistinctTagByTextIgnoreCase(text);
        if(!tagList.isEmpty()){
            return tagList.get(0);
        } else{
            return null;
        }
    }


    public void saveTag(Tag tag){
        tagRepo.save(tag);
    }
    
    //methode zoek tag krijgt string mee. Met string vraag je in tagrepo de tags die die string hebben als text. Met die tag kun je aan bookrepo vragen welke boeken horen bij deze tag

}

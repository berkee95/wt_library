package wt.library.controller;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import wt.library.model.BookCopy;

import java.util.Collection;

@Component
public interface BookCopyRepository extends JpaRepository<BookCopy, Long>{
   // List<BookCopy> findDistinctBookCopyByIsbn(String isbn);

//    List<BookCopy> unreservedBooks(long bookId);

     @Query(value = "SELECT * FROM book_copy WHERE reserved = 0 AND book_id = :#{#bookId}", nativeQuery = true)
     Collection<BookCopy> unreservedBooks(@Param("bookId") long bookId);
    
     @Query(value = "SELECT * FROM book_copy WHERE reserved = 1 AND book_id = :#{#bookId}", nativeQuery = true)
     Collection<BookCopy> reservedBooks(@Param("bookId") long bookId);
    

}

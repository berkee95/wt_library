package wt.library.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wt.library.model.*;
import wt.library.model.BookCopy;
import wt.library.model.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class BookService {
    @Autowired
    private BookRepository bookRepo;
    @Autowired
    private BookCopyRepository bookCopyRepo;

    @Autowired 
    private TagService tagService;
    
    public Book findBookById(long id){
        Optional<Book> bookOptional = bookRepo.findById(id);
        if(bookOptional.isPresent()){
            return bookOptional.get();
        }
        return null;
    }
  /*
    public Collection <Book> selectAllBooks(){
        return bookRepo.selectAllBooks();
    }

    public Collection <Book> selectBooksByAuthor(String authorName){
        return bookRepo.selectBooksByAuthor(authorName);
    }

    public Collection <Book> selectBooksByTitle(String bookTitle){
        return bookRepo.selectBooksByTitle(bookTitle);
    }
*/
    public List<Book> find10ByAuthor(String bookAuthor){
        return bookRepo.findFirst10ByAuthor(bookAuthor);
    }

    public List<Book> findBookByAuthor(String bookAuthor){
        return bookRepo.findDistinctBookByAuthorIgnoreCase(bookAuthor);
    }
    public List<Book> findFirst10(){
        return bookRepo.findFirst10ByOrderById();
    }
    public List<Book> findBookByTitle(String bookTitle){
        return bookRepo.findDistinctBookByTitleIgnoreCase(bookTitle);
    }
    public List<Book> findBookByTitleOrAuthor(String bookTitle, String bookAuthor){
        return bookRepo.findDistinctBookByAuthorOrTitleAllIgnoreCase(bookTitle, bookAuthor);
    }
    public List<Book> findBooks(String bookTitle, String bookAuthor, String bookIsbn) {
        return bookRepo.findDistinctBookByAuthorOrTitleOrIsbnAllIgnoreCase(bookTitle, bookAuthor, bookIsbn);
    }
    public List<Book> findAllBooks(){
        return bookRepo.findAll();
    }
   
  
    public void saveBook(Book book){
        bookRepo.save(book);
    }



    public void reserveBook(Book book, User user){
        long id = book.getId();
        Collection<BookCopy> coll = bookCopyRepo.unreservedBooks(id);
        System.out.println(coll);
        List<BookCopy> bookCopyList = new ArrayList<BookCopy>(coll);
        
        try{
            BookCopy copy = bookCopyList.get(0);
            copy.setReserved(true);
            bookCopyRepo.save(copy);
            //System.out.println("Reserved op true gezet");
        }
        catch(Exception e){
            System.out.println("Er is geen copy");
        }
    }

    


    public void addTagToBook(long tagId, long bookId){
        System.out.println(tagId);
        System.out.println(bookId);
        Tag tag = tagService.findTagById(tagId);
        Book book = findBookById(bookId);
        System.out.println(tag.getText());
        System.out.println(book.getAuthor());
        
        System.out.println(book.getTags());
        book.addTag(tag);
        
        bookRepo.save(book);
        
       
    }
    /*
    public void addTagToBook(long tagId, long bookId){
        Tag tag = tagService.findTagById(tagId);
        Book book = findBookById(bookId);
        //tag.addBook(book);
        //System.out.println(tag.getBooks().get(0));

        book.addTag(tag);
        //List<Book> books = tag.getBooks();
        //books.add(book);
        //tag.setBooks(books);
        //tagRepo.save(tag);
     
        bookRepo.save(book);
    }
    */
}

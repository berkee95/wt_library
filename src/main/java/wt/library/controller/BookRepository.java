package wt.library.controller;

import org.springframework.data.jpa.repository.JpaRepository;
import wt.library.model.Book;

import java.util.List;

public interface BookRepository extends JpaRepository<Book, Long>{
    //@Query("DELETE * FROM book")
    //void deleteBook();

    List<Book> findAll();

    List<Book> findFirst10ById(long bookId);
   
    List <Book> findFirst10ByAuthor(String bookAuthor);
    
    List<Book> findDistinctBookByAuthorIgnoreCase(String bookAuthor);

    List<Book> findDistinctBookByTitleIgnoreCase(String bookTitle);

    List<Book> findDistinctBookByAuthorOrTitleAllIgnoreCase(String bookAuthor, String bookTitle);

 

    List<Book> findDistinctBookByAuthorOrTitleOrIsbnAllIgnoreCase(String bookAuthor,
                                                                        String bookTitle,
                                                                        String isbn);

    List<Book> findFirst10ByOrderById();

    //@Query(value = "SELECT * FROM book", nativeQuery = true)
    //Collection <Book> selectAllBooks();

    // @Query(value = "SELECT * FROM book WHERE author = :#{#bookAuthor} ", nativeQuery = true)
    // Collection <Book> selectBooksByAuthor(@Param("bookAuthor") String bookAuthor);

    // @Query(value = "SELECT * FROM book WHERE title = :#{#bookTitle} ", nativeQuery = true)
    // Collection <Book> selectBooksByTitle(@Param("bookTitle") String bookTitle);

}

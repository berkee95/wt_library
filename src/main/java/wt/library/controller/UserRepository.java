package wt.library.controller;

import org.springframework.data.jpa.repository.JpaRepository;

import wt.library.model.User;
import org.springframework.stereotype.Component;

@Component
public interface UserRepository extends JpaRepository<User, Long>{

}

package wt.library.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wt.library.model.BookCopy;
import wt.library.model.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
@Service
public class BookCopyService {
    @Autowired
    private BookCopyRepository bookCopyRepo;

    public BookCopy findbookCopyById(long id){
        Optional<BookCopy> bookCopyOptional = bookCopyRepo.findById(id);
        if(bookCopyOptional.isPresent()){
            return bookCopyOptional.get();
        }
        return null;
    }

    /*
    public List<BookCopy> findBookCopyByIsbn(String isbn){
        return bookCopyRepo.findDistinctBookCopyByIsbn(isbn);
    }
    */

    public List<BookCopy> showReservedCopies(long bookId){
        
        Collection<BookCopy> coll = bookCopyRepo.reservedBooks(bookId);

        List<BookCopy> bookCopyList = new ArrayList<BookCopy>(coll);
        return bookCopyList;
    }
  
    public void savebookCopy(BookCopy bookCopy){
        bookCopyRepo.save(bookCopy);
    }



}


package wt.library.controller;

import org.springframework.data.jpa.repository.JpaRepository;
import wt.library.model.Tag;

import java.util.List;

public interface TagRepository extends JpaRepository<Tag, Long> {

    List<Tag> findDistinctTagByTextIgnoreCase(String bookTitle);

}
